# Unidad 2: Programación Básica

## **1. Introducción**

Ejercicio a resolver:

Desarrolla una aplicación que permita resolver el siguiente ejercicio. Recuerda incluir las instrucciones y la pantalla de salida.

Cree una clase llamada Factura, que una ferretería podría utilizar para representar un artículo vendido. Una Factura debe incluir cuatro piezas de información como variables de instancia: un número de pieza (tipo String), la descripción de la pieza (tipo String), la cantidad de artículos de ese tipo que se van a comprar (tipo int) y el precio por artículo (double). Su clase debe tener un constructor que inicialice las cuatro variables de instancia. Proporcione un método establecer y un método obtener para cada variable de instancia. Además, proporcione un método llamado obtenerMontoFactura, que calcule el monto de la factura (es decir, que multiplique la cantidad por el precio por artículo) y después devuelva ese monto como un valor double. Si la cantidad no es positiva, debe establecerse en 0. Si el precio por artículo no es positivo, debe establecerse a 0.0. Escriba una aplicación de prueba llamada Prueba-Factura, que demuestre las capacidades de la clase Factura.

Requerimientos necesarios en tu computadora:
- Permisos de administrador para descargar e intalar software.
- Chocolatey package manager (sólo en Windows y es opcional).
- Homebrew (MAC y es opcional).
- Java JDK >1.8.
- Maven 3.5.
- IntelliJ, Eclipse Luna o cualquier otro IDE para java.

## **2. Herramientas**

Los lenguajes y lenguaje de programación utilizados en este projecto son:

1. Java como lenguaje de programación.
2. TestNG como herramienta de pruebas unitarias para ejecutar las pruebas.
3. Maven como herramienta para manejo de dependencias.
4. Git software de control de versiones.

## **3. Instalación de Herramientas**

Si tu equipo ya cuenta con Java 1.8 o mayor y Maven 3.4 o mayor, puedes omitir esta sección.

**Windows**
**Instalación de Chocolatey**
Visita la página https://chocolatey.org/install y sigue los pasos de instalación.

**Instalación de Java**
1. Abre una linea de comandos como administrador.
2. Copia y pega en terminal lo siguiente: ```choco install jdk8 -y```.
3. Espera a que se completen los comandos. 
4. Si no vez algún error ejecuta el comando ```refreshenv```. 
5. Listo para usar Java .

**Instalación de Maven**
1. Abre una linea de comandos como administrador.
2. Copia y pega en terminal lo siguiente: ```choco install maven -y```.
3. Espera a que se completen los comandos. 
4. Si no vez algún error ejecuta el comando ```refreshenv```.
5. Listo para usar Maven.

**Instalación de Git**
1. Abre una linea de comandos como administrador.
2. Copia y pega en terminal lo siguiente: ```choco install git -y```
3. Espera a que se completen los comandos. 
4. Si no vez algún error ejecuta el comando ```refreshenv```.
5. Listo para usar Git.

**MAC**
**Instalación de Homebrew**
Visita la página https://docs.brew.sh/Installation y sigue los pasos de instalación.

**Instalación de Java**
1. Abre una terminal.
2. Copia,pega y ejecuta en terminal.
```
brew update
brew install java
```
3. Espera a que se completen los comandos. 
4. Si no vez algún error estás listo para usar Java.

**Instalación de Maven**
1. Abre una terminal.
2. Copia,pega y ejecuta en terminal.
```
brew update
brew install maven
```
3. Espera a que se completen los comandos. 
4. Si no vez algún error estás listo para usar Maven.

**Instalación de Git**
1. Abre una terminal.
2. Copia,pega y ejecuta en terminal.
```
brew update
brew install git
```
3. Espera a que se completen los comandos. 
4. Si no vez algún error estás listo para usar Git.

## **3. Estructura del Proyecto**

Dentro de src >main >java >org >davinci >unidaddos; encontrarás la clase ```Factura``` donde se encuentran todos los atributos y métodos (getters y setter).

En el folder src >test >java >org >davinci >unidaddos, se encuentra la clase ```PruebaFactura```para ejecutar pruebas.

**Directorio**

    ├── src                                         # Archivos fuente
    │   ├── main                       
    |       ├── java
    |           ├── org
    |               ├── davinci
    |                   ├── unidaddos
    |                       ├── Factura.java         # Clase con atributos y métodos para Factura 
    |   ├── test                                     
    |       ├── java
    |           ├── org
    |               ├── davinci
    |                   ├── unidaddos
    |                       ├── PruebaFactura.java   # Clases para pruebas  
    ├── .gitignore                                   # Ignorar archivos              
    ├── pom.xml                                      # Archivo con dependencias
    └── README.md                                    
    └── testng.xml                                   #Archivo para poder ejecutar las pruebas en el pom

## **4. Ejecución del Proyecto y Resultados**

1. Abre una terminal o linea de comandos.
2. Clona el repositorio copiando, pegando y ejecutando lo siguiente:  
```git clone https://gsanchezm@bitbucket.org/msc_davinci/pb_unidad2.git```.
3. En la terminal o linea de comando ya abierta ejecuta los siguientes comandos:
```
mvn clean
mvn validate
mvn compile
mvn test
```  
5. La ejecución mostrará los resultados dw las pruebas del software.

## **5. Casos de Uso y Datos de Prueba**

Los datos de prueba se encuentran en el DataProvider de la siguiente forma:
```
@DataProvider(name = "DatosFactura")
    public Object[][] getData(Method method){
        if(method.getName().equals("obtenerFacturaValida")){
            return new Object[][]{
                    {"Tornillo-0012", "12 pulgadas", 15, 1.50, 22.5},
                    {"Perno-1078", "7 pulgadas", 2, 3.80, 7.6},
                    {"Desarmador-0052", "Marca Barren", 1, 34.50, 34.5}
            };
        }else if(method.getName().equals("datosFacturaInvalidos")){
            return new Object[][]{
                    {"", "12 pulgadas", 15, 1.50, 22.5},
                    {"Perno-1078", "", 2, 3.80, 7.6},
                    {"Desarmador-0052", "Marca Barren", -1, 34.50, .0},
                    {"Perno-1078", "7 pulgadas", 2, -3.80, .0},
            };
        }

        return null;
    }
```
Donde los primeros 3 datos son para probar que el comportamiento del software sea el ideal,
```
if(method.getName().equals("obtenerFacturaValida")){
    return new Object[][]{
        {"Tornillo-0012", "12 pulgadas", 15, 1.50, 22.5},
        {"Perno-1078", "7 pulgadas", 2, 3.80, 7.6},
        {"Desarmador-0052", "Marca Barren", 1, 34.50, 34.5}
    };
}
```
y el segundo set de datos servirá para validad números negativos y string vacíos
```
else if(method.getName().equals("datosFacturaInvalidos")){
    return new Object[][]{
        {"", "12 pulgadas", 15, 1.50, 22.5},
        {"Perno-1078", "", 2, 3.80, 7.6},
        {"Desarmador-0052", "Marca Barren", -1, 34.50, .0},
        {"Perno-1078", "7 pulgadas", 2, -3.80, .0},
    };
}
```
**Pruebas para comprobar el comportamiento ideal**

El _Assert_ validará que el resultado obtenido del total, sea el mismo que el resultado esperado.

```
@Test(dataProvider = "DatosFactura", priority = 0,
          description = "Validar que todo se ejecute correctamente")
    public void obtenerFacturaValida(String pieza, String descripción, int cantidad, double precio, double total){
        factura = new Factura(pieza, descripción, cantidad, precio);
        System.out.println("\n    ----  FACTURA  ---- \n" + factura);
        factura.obtenerMontoFactura();
        Assert.assertEquals(factura.getTotal(), total);
    }
```

**Pruebas para validar números negativos y string vacios**

El _Assert_ validará que el resultado obtenido del total, sea el mismo que el resultado esperado.

```
@Test(dataProvider = "DatosFactura", priority = 1,
          description = "Validar que cuando precio y/o cantidad sean números negativos, total sea 0")
    public void datosFacturaInvalidos(String pieza, String descripción, int cantidad, double precio, double total){
        factura = new Factura(pieza, descripción, cantidad, precio);
        System.out.println("\n    ----  FACTURA  ---- \n" + factura);
        factura.obtenerMontoFactura();
        Assert.assertEquals(factura.getTotal(), total);
    }
```

## **6. Resultados de Ejecución**

Métodos ejecutados con los datos de entrada provenientes del DataProvider
Consola IntelliJ

![image](https://user-images.githubusercontent.com/24705055/47133165-13c8e000-d26d-11e8-8a79-ab2f21ddcafb.png)

Consola Maven

![image](https://user-images.githubusercontent.com/24705055/47133217-5985a880-d26d-11e8-8d3e-daef6a61a17a.png)

Resultado del funcionamiento correcto del software

![image](https://user-images.githubusercontent.com/24705055/47133278-b1bcaa80-d26d-11e8-8027-ff72f3e1e006.png)

Resultado cuando existen strings vacios o números negativos

![image](https://user-images.githubusercontent.com/24705055/47133301-cb5df200-d26d-11e8-876e-a776aa4b31c1.png)