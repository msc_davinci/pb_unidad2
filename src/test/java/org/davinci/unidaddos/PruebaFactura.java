package org.davinci.unidaddos;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class PruebaFactura {

    Factura factura;

    @Test(dataProvider = "DatosFactura", priority = 0,
          description = "Validar que todo se ejecute correctamente")
    public void obtenerFacturaValida(String pieza, String descripcion, int cantidad, double precio, double total){
        factura = new Factura(pieza, descripcion, cantidad, precio);
        System.out.println("\n    ----  FACTURA  ---- \n" + factura);
        factura.obtenerMontoFactura();
        Assert.assertEquals(factura.getTotal(), total);
    }

    @Test(dataProvider = "DatosFactura", priority = 1,
          description = "Validar que cuando precio y/o cantidad sean números negativos, total sea 0")
    public void datosFacturaInvalidos(String pieza, String descripcion, int cantidad, double precio, double total){
        factura = new Factura(pieza, descripcion, cantidad, precio);
        System.out.println("\n    ----  FACTURA  ---- \n" + factura);
        factura.obtenerMontoFactura();
        Assert.assertEquals(factura.getTotal(), total);
    }

    @DataProvider(name = "DatosFactura")
    public Object[][] getData(Method method){
        if(method.getName().equals("obtenerFacturaValida")){
            return new Object[][]{
                    {"Tornillo-0012", "12 pulgadas", 15, 1.50, 22.5},
                    {"Perno-1078", "7 pulgadas", 2, 3.80, 7.6},
                    {"Desarmador-0052", "Marca Barren", 1, 34.50, 34.5}
            };
        }else if(method.getName().equals("datosFacturaInvalidos")){
            return new Object[][]{
                    {"", "12 pulgadas", 15, 1.50, 22.5},
                    {"Perno-1078", "", 2, 3.80, 7.6},
                    {"Desarmador-0052", "Marca Barren", -1, 34.50, .0},
                    {"Perno-1078", "7 pulgadas", 2, -3.80, .0},
            };
        }

        return null;
    }
}


