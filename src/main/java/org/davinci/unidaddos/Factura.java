package org.davinci.unidaddos;

import java.text.DecimalFormat;

public class Factura {
    private String numeroPieza;
    private String descripcionPieza;
    private int cantidad;
    private double precio;
    private double total;

    public Factura(String numeroPieza, String descripcionPieza, int cantidad, double precio) {
        setNumeroPieza(numeroPieza);
        setDescripcionPieza(descripcionPieza);
        setCantidad(cantidad);
        setPrecio(precio);
    }

    //Métodos establecer y obtener de pieza
    public String getNumeroPieza() {
        return numeroPieza;
    }

    public void setNumeroPieza(String numeroPieza) {
        this.numeroPieza = (numeroPieza.isEmpty())?"Pieza Genérica":numeroPieza;
    }

    //Métodos establecer y obtener de descripción de la pieza
    public String getDescripcionPieza() {
        return descripcionPieza;
    }

    public void setDescripcionPieza(String descripcionPieza) {
        this.descripcionPieza = (descripcionPieza.isEmpty())?"Pieza Genérica":descripcionPieza;
    }

    //Métodos establecer y obtener de cantidad de piezas
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        //if (cantidad == 0) {
        //}
        try{
            this.cantidad = (cantidad < 0)?0:cantidad;
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    //Métodos establecer y obtener de precio de la pieza
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        try{
            this.precio = (precio < 0)?0.0:precio;
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

    }

    public double getTotal(){
        return (getPrecio()*getCantidad());
    }

    public void obtenerMontoFactura(){
        System.out.printf("El total a pagar es: %s", new DecimalFormat(".##").format(getTotal()));
    }

    public String toString(){
        return "Pieza: " + getNumeroPieza() +
                "\nDescripción: " + getDescripcionPieza() +
                "\nCantidad: " + getCantidad() +
                "\nPrecio: " + new DecimalFormat("####.##").format(getPrecio()) + "\n";
    }
}
